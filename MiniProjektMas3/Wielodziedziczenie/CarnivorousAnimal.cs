﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniProjektMas3.Wielodziedziczenie
{
    public enum PhysicalActivityLevel
    {
        Low,
        Medium,
        High
    }

    /// <summary>
    /// Animal which eat a food
    /// </summary>
    public class CarnivorousAnimal : Animal, ICarnivorousAnimal
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="CarnivorousAnimal"/> class.
        /// </summary>
        /// <param name="activityLevel">
        /// The activity level.
        /// </param>
        public CarnivorousAnimal(PhysicalActivityLevel activityLevel, string name, int weight, bool isDangerous) : base(name, weight, isDangerous)
        {
            this.ActivityLevel = activityLevel;
        }

        /// <summary>
        /// Gets or sets the activity level.
        /// </summary>
        public PhysicalActivityLevel ActivityLevel { get; set; }

        /// <inheritdoc />
        public override int GetAvarageDayConsumptionInKg()
        {
            switch (this.ActivityLevel)
            {
                case PhysicalActivityLevel.Low:
                    return (int)(this.Weight * 0.10);
                case PhysicalActivityLevel.Medium:
                    return (int)(this.Weight * 0.25);
                case PhysicalActivityLevel.High:
                    return (int)(this.Weight * 0.5);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}
