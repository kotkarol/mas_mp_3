﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniProjektMas3.Wielodziedziczenie
{
    public interface IHerbivoreAnimal
    {
        double FavorituePlantWeight { get; set; }
    }
}
