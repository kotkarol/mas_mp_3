﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniProjektMas3.Wielodziedziczenie
{
    /// <summary>
    /// The omnivorous animal.
    /// </summary>
    public class OmnivorousAnimal : CarnivorousAnimal , IHerbivoreAnimal
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OmnivorousAnimal"/> class.
        /// </summary>
        /// <param name="activityLevel">
        /// The activity level.
        /// </param>
        /// <param name="avarageHoursSpendPerDayAtEating">
        /// The avarage Hours Spend Per Day At Eating.
        /// </param>
        public OmnivorousAnimal(PhysicalActivityLevel activityLevel, int avarageHoursSpendPerDayAtEating, string name, int weight, bool isDangerous) : base(activityLevel, name, weight, isDangerous)
        {
            this.AverageHoursSpendPerDayAtEating = avarageHoursSpendPerDayAtEating;
        }

        /// <inheritdoc />
        public double FavorituePlantWeight { get; set; }

        /// <summary>
        /// Gets the average hours spend per day at eating.
        /// </summary>
        public int AverageHoursSpendPerDayAtEating { get; }
    }
}
