﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniProjektMas3.Wielodziedziczenie
{
    public abstract class Animal
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Animal"/> class.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="weight">
        /// The weight.
        /// </param>
        /// <param name="isDangerous">
        /// The is dangerous.
        /// </param>
        public Animal(string name, int weight, bool isDangerous)
        {
            this.Name = name;
            this.Weight = weight;
            this.IsDangerous = isDangerous;
        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Gets the weight.
        /// </summary>
        public int Weight { get; }

        /// <summary>
        /// Gets a value indicating whether is dangerous.
        /// </summary>
        public bool IsDangerous { get; }

        /// <summary>
        /// Amount of food consumed by animal per day.
        /// </summary>
        /// <returns>Amount of food in kilograms per day</returns>
        public abstract int GetAvarageDayConsumptionInKg();
    }
}
