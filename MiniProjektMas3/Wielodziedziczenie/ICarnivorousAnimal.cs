﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniProjektMas3.Wielodziedziczenie
{
    public interface ICarnivorousAnimal
    {
        /// <summary>
        /// Level of physical activity of animal
        /// </summary>
        PhysicalActivityLevel ActivityLevel { get; set; }
    }
}
