﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniProjektMas3.Wielodziedziczenie
{
    /// <summary>
    /// The herbivore animal.
    /// </summary>
    public class HerbivoreAnimal : Animal, IHerbivoreAnimal
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="HerbivoreAnimal"/> class.
        /// </summary>
        /// <param name="favorituePlantWeight">
        /// The favoritue plant weight.
        /// </param>
        public HerbivoreAnimal(double favorituePlantWeight, string name, int weight, bool isDangerous) : base(name, weight, isDangerous)
        {
            this.FavorituePlantWeight = favorituePlantWeight;
        }

        /// <summary>
        /// Gets or sets the favorite plant weight.
        /// </summary>
        public double FavorituePlantWeight { get; set; }

        /// <inheritdoc />
        public override int GetAvarageDayConsumptionInKg()
        {
            return (int)(this.Weight * this.FavorituePlantWeight);
        }
    }
}
