﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniProjektMas3.Interfaces.IMaterial
{
    public interface IMaterial
    {
        int Weight { get; set; }
        string Name { get; set; }
    }
}
