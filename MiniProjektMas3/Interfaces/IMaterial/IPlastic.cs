﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniProjektMas3.Interfaces
{
    public interface IPlastic : IMaterial.IMaterial
    {
        int Hardness { get; set; }
        string Color { get; set; }
    }
}
