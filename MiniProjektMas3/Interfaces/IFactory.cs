﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MiniProjektMas3.Interfaces.IMaterial;
using MiniProjektMas3.Models;
using MiniProjektMas3.Models.Material;

namespace MiniProjektMas3.Interfaces
{
    public interface IPlasticFactory<T>
    {
        T ProducePlasticItem(PlasticMaterial material, T thingToProduce);
    }

    public interface ISteelFactory<T> where  T : Steel
    {
        T ProduceSteelItem(SteelMaterial material, T thingToProduce);
    }
}
