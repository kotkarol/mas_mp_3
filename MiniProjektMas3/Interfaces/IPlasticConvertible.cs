﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MiniProjektMas3.Models;

namespace MiniProjektMas3.Interfaces
{
    public interface IPlasticConvertible<T> where T : IPlastic
    {
        T[] Convert();
    }
}
