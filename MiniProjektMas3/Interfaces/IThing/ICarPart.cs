﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniProjektMas3.Interfaces.IThing
{
    public interface ICarPart : IThing
    {
        string CarPartName { get; set; }
        string CarModel { get; set; }
        string CarManufacturer { get; set; }
    }
}
