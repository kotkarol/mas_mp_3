﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniProjektMas3.Interfaces.IThing
{
    public interface ITool : IThing
    {
        string Name { get; set; }
        string Purpose { get; set; }
    }
}
