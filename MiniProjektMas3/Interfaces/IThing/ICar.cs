﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniProjektMas3.Interfaces.IThing
{
    public interface ICar : IThing
    {
        string Model { get; set; }
        string Manufacturer { get; set; }
    }
}
