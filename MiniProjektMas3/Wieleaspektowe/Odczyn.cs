﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniProjektMas3.Wieleaspektowe
{
    public abstract class Odczyn
    {
        public double Ph { get; }

        public Odczyn(double ph)
        {
            this.Ph = ph;
        }
    }
}
