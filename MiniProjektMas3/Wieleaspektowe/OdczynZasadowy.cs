﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniProjektMas3.Wieleaspektowe
{
    public class OdczynZasadowy : Odczyn
    {
        public string SposobBadaniaPh { get; }

        public string NazwaProduktuZawierajacego { get; }

        public OdczynZasadowy(double ph, string sposobBadaniaPh, string nazwaProduktuZawierajacego)
            : base(ph)
        {
            this.SposobBadaniaPh = sposobBadaniaPh;
            this.NazwaProduktuZawierajacego = nazwaProduktuZawierajacego;
        }
    }
}
