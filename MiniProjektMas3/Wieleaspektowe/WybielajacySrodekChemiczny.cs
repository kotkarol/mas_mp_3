﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniProjektMas3.Wieleaspektowe
{
    public class WybielajacySrodekChemiczny : SrodekChemiczny
    {
        public bool NiebezpiecznyDlaSkory { get; }

        public WybielajacySrodekChemiczny(string nazwa, 
                                          string sklad, 
                                          DateTime dataWynalezienia, 
                                          Odczyn odczyn,
                                          bool niebezpiecznyDlaSkory)
            : base(nazwa, sklad, dataWynalezienia, odczyn)
        {
            this.NiebezpiecznyDlaSkory = niebezpiecznyDlaSkory;
        }
    }
}
