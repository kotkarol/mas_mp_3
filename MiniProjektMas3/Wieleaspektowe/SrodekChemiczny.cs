﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniProjektMas3.Wieleaspektowe
{
    public abstract class SrodekChemiczny
    {
        public string Nazwa { get; }

        public string Sklad { get; }

        public DateTime DataWynalezienia { get; }

        public Odczyn Odczyn { get; }

        public SrodekChemiczny(string nazwa, string sklad, DateTime dataWynalezienia, Odczyn odczyn)
        {
            this.Nazwa = nazwa;
            this.Sklad = sklad;
            this.DataWynalezienia = dataWynalezienia;
            this.Odczyn = odczyn;
        }
    }
}
