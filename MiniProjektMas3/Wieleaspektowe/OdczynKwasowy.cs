﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniProjektMas3.Wieleaspektowe
{
    public class OdczynKwasowy : Odczyn
    {
        public string Kolor { get; }

        public bool NiszczySzklo { get; }

        public OdczynKwasowy(double ph, string kolor, bool niszczySzklo) : base(ph)
        {
            this.Kolor = kolor;
            this.NiszczySzklo = niszczySzklo;
        }
    }
}
