﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniProjektMas3.Wieleaspektowe
{
    public class CzyszczacySrodekChemiczny : SrodekChemiczny
    {
        public DateTime DataWprowadzeniaNaRynek { get; }

        public DateTime DataUzyskaniaCertyfikatu { get; }

        public CzyszczacySrodekChemiczny(string nazwa, 
                                         string sklad, 
                                         DateTime dataWynalezienia,
                                         Odczyn odczyn, 
                                         DateTime dataWprowadzeniaNaRynek, 
                                         DateTime dataUzyskaniaCertyfikatu)
            : base(nazwa, sklad, dataWynalezienia, odczyn)
        {
            this.DataWprowadzeniaNaRynek = dataWprowadzeniaNaRynek;
            this.DataUzyskaniaCertyfikatu = dataUzyskaniaCertyfikatu;
        }
    }
}
