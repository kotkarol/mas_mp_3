﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniProjektMas3.Models
{
    public enum Role
    {
        JavaProgrammer,
        SqlProgrammer,
        JavascriptProgrammer
    }
    public class Osoba
    {
        /// <summary>
        /// The roles.
        /// </summary>
        List<Role> roles = new List<Role>();

        /// <summary>
        /// The favorite intelij extension.
        /// </summary>
        private string favoritueIntelijExtension;

        /// <summary>
        /// The favorite sql dialect.
        /// </summary>
        private string favoritueSqlDialect;

        /// <summary>
        /// The favorite framework.
        /// </summary>
        private string favoritueFramework;

        public string Name { get; }

        public string FavoritueIntelijExtension
        {
            get
            {
                VerifyRole(Role.JavaProgrammer);
                return this.favoritueIntelijExtension;
            }
            set
            {
                VerifyRole(Role.JavaProgrammer);
                this.favoritueIntelijExtension = value;
            }
        }

        public string FavoritueSqlDialect
        {
            get
            {
                VerifyRole(Role.SqlProgrammer);
                return this.favoritueSqlDialect;
            }
            set
            {
                VerifyRole(Role.SqlProgrammer);
                this.favoritueSqlDialect = value;
            }
        }



        public string FavoritueFramework
        {
            get
            {
                VerifyRole(Role.JavascriptProgrammer);
                return this.favoritueFramework;
            }
            set
            {
                VerifyRole(Role.JavascriptProgrammer);
                this.favoritueFramework = value;
            }
        }

        private void VerifyRole(Role role)
        {
            if (!HasRole(role))
            {
                throw new Exception("Current Person is not a: " + Role.JavaProgrammer);
            }
        }

        public Osoba(string name)
        {
            Name = name;
        }

        public void AddRole(Role role)
        {
            if (!roles.Contains(role))
            {
                roles.Add(role);
            }
        }

        public void DeleteRole(Role role)
        {
            if (roles.Contains(role))
            {
                roles.Remove(role);
            }
        }

        public bool HasRole(Role role)
        {
            return roles.Contains(role);
        }
    }
}
