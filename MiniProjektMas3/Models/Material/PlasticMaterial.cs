﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MiniProjektMas3.Interfaces;

namespace MiniProjektMas3.Models
{
    public class PlasticMaterial : Plastic
    {
        public PlasticMaterial(double weight, int hardness, string color)
        {
            this.Weight = (int)weight;
            this.Hardness = hardness;
            this.Color = color;
        }

        public PlasticMaterial(int hardness, string color)
        {
            this.Hardness = hardness;
            this.Color = color;
        }

        public override int CalculateNeededAmountForUnit()
        {
            return 1;
        }
    }

}
