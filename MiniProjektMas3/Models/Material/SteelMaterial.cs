﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MiniProjektMas3.Interfaces.IMaterial;

namespace MiniProjektMas3.Models.Material
{
    public class SteelMaterial : ISteel
    {
        public int QualityLevel { get; set; }
        public string Grade { get; set; }

        public int Amount { get; set; }
        public int Weight { get; set; }
        public string Name { get; set; }
    }
}
