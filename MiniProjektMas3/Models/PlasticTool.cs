﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MiniProjektMas3.Interfaces;

namespace MiniProjektMas3.Models
{
    public class PlasticTool : Plastic, ITool, IPlasticConvertible<PlasticMaterial>
    {
        public override int CalculateNeededAmountForUnit()
        {
            return this.Hardness * this.Weight;
        }

        public string Name { get; set; }
        public string Purpose { get; set; }
        public PlasticMaterial[] Convert()
        {
            List<PlasticMaterial> pms = new List<PlasticMaterial>();
            PlasticMaterial pm = new PlasticMaterial(this.Hardness, this.Color);

            for (int i = 0; i < this.Weight / pm.CalculateNeededAmountForUnit(); i++)
            {
                pms.Add(new PlasticMaterial(pm.CalculateNeededAmountForUnit(), this.Hardness, this.Color));
            }

            return pms.ToArray();
        }
    }
}
