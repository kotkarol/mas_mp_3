﻿using System;
using MiniProjektMas3.Interfaces;
using MiniProjektMas3.Models.Material;

namespace MiniProjektMas3.Models
{
    public class Factory : IPlasticFactory<PlasticTool>, ISteelFactory<GearBox>
    {

        public PlasticTool ProducePlasticItem(PlasticMaterial material, PlasticTool thingToProduce)
        {
            if (material.Weight < thingToProduce.CalculateNeededAmountForUnit())
            {
                throw new Exception("Cannot produce item! There isn't enough steel!");
            }

            material.Weight -= thingToProduce.CalculateNeededAmountForUnit();
            return new PlasticTool()
            {
                Color = thingToProduce.Color,
                Hardness = thingToProduce.Hardness,
                Name = thingToProduce.Name,
                Purpose = thingToProduce.Purpose,
                Weight = thingToProduce.Weight
            };

        }

        public GearBox ProduceSteelItem(SteelMaterial material, GearBox thingToProduce)
        {
            if (material.Amount < thingToProduce.CalculateNeededAmountForUnit())
            {
                throw new Exception("Cannot produce item! There isn't enough steel!");
            }

            material.Amount -= thingToProduce.CalculateNeededAmountForUnit();
            return new GearBox()
            {
                CarManufacturer = thingToProduce.CarManufacturer,
                CarModel = thingToProduce.CarModel,
                CarPartName = thingToProduce.CarPartName
            };
        }
    }
}
