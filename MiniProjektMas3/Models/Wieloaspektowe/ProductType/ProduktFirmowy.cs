﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniProjektMas3.Models.Wieloaspektowe.ProductType
{
    public class ProduktFirmowy : Product
    {
        public int MinimalnaWielkoscOrganizacji { get; set; }
        public bool NaliczacRabat { get; set; } 
       
    }
}
