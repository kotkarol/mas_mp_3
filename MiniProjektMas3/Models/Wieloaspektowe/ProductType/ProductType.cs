﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniProjektMas3.Models.Wieloaspektowe
{
    public abstract class ProductType
    {
        public string ProductTypeName { get; set; }
    }
}
