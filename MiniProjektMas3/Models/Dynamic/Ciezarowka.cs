﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniProjektMas3.Models.Dynamic
{
    public class Ciezarowka : PlatformaSamochodowa
    {
        public int IloscOsi { get; }

        public Ciezarowka(double szerokosc, double wysokosc, double waga, int iloscOsi)
            : base(szerokosc, wysokosc, waga)
        {
            this.IloscOsi = iloscOsi;
        }

        public Ciezarowka(PlatformaSamochodowa platforma, int iloscOsi) : base(platforma.Szerokosc, platforma.Wysokosc, platforma.Wysokosc)
        {
            this.IloscOsi = iloscOsi;
        }

        public override string GetCarSegment()
        {
            return "D";
        }
    }
}
