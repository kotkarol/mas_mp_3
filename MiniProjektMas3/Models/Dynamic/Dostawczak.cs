﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniProjektMas3.Models.Dynamic
{
    public class Dostawczak : PlatformaSamochodowa
    {
        public double Ladownosc { get; }

        public Dostawczak(double szerokosc, double wysokosc, double waga, double ladownosc)
            : base(szerokosc, wysokosc, waga)
        {
            this.Ladownosc = ladownosc;
        }

        public Dostawczak(PlatformaSamochodowa platforma, double ladownosc) : base (platforma.Szerokosc, platforma.Wysokosc, platforma.Wysokosc)
        {
            this.Ladownosc = ladownosc;
        }

        public override string GetCarSegment()
        {
            return "C";
        }
    }
}
