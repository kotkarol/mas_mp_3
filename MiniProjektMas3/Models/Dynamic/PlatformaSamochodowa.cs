﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniProjektMas3.Models.Dynamic
{
    public abstract class PlatformaSamochodowa
    {
        public double Szerokosc { get; private set; }

        public double Wysokosc { get; private set; }

        public double Waga { get; private set; }

        public PlatformaSamochodowa(double szerokosc, double wysokosc, double waga)
        {
            this.Szerokosc = szerokosc;
            this.Wysokosc = wysokosc;
            this.Waga = waga;
        }
        public abstract string GetCarSegment();
    }
}
