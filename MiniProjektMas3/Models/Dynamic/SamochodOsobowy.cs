﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniProjektMas3.Models.Dynamic
{
    public class SamochodOsobowy : PlatformaSamochodowa
    {
        public int LiczbaMiejsc { get; }

        public SamochodOsobowy(double szerokosc, double wysokosc, double waga, int liczbaMiejsc)
            : base(szerokosc, wysokosc, waga)
        {
            this.LiczbaMiejsc = liczbaMiejsc;
        }

        public SamochodOsobowy(PlatformaSamochodowa platforma, int liczbaMiejsc) : base(platforma.Szerokosc, platforma.Wysokosc, platforma.Wysokosc)
        {
            this.LiczbaMiejsc = liczbaMiejsc;
        }

        public override string GetCarSegment()
        {
            return "B";
        }
    }
}
