﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MiniProjektMas3.Interfaces;

namespace MiniProjektMas3.Models
{
    public class PlasticCarPart : Plastic, IPlasticConvertible<PlasticToy>, IPlasticConvertible<PlasticMaterial>
    {
        public PlasticCarPart(int hardness, string color)
        {
            this.Hardness = hardness;
            this.Color = color;
        }

        public PlasticCarPart(int weight, int hardness, string color)
        {
            this.Weight = weight;
            this.Hardness = hardness;
            this.Color = color;
        }

        public override int CalculateNeededAmountForUnit()
        {
            if (this.Color == "black" && this.Hardness > 12)
            {
                return 25;
            }

            return 35;
        }

        public PlasticToy[] Convert()
        {
            List<PlasticToy> pms = new List<PlasticToy>();
            PlasticToy pm = new PlasticToy(this.Hardness, this.Color);

            for (int i = 0; i < this.Weight / pm.CalculateNeededAmountForUnit(); i++)
            {
                pms.Add(new PlasticToy(pm.CalculateNeededAmountForUnit(), this.Hardness, this.Color));
            }

            return pms.ToArray();
        }

        PlasticMaterial[] IPlasticConvertible<PlasticMaterial>.Convert()
        {
            List<PlasticMaterial> pms = new List<PlasticMaterial>();
            PlasticToy pm = new PlasticToy(this.Hardness, this.Color);

            for (int i = 0; i < this.Weight / pm.CalculateNeededAmountForUnit(); i++)
            {
                pms.Add(new PlasticMaterial(pm.CalculateNeededAmountForUnit(), this.Hardness, this.Color));
            }

            return pms.ToArray();
        }
    }
}
