﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniProjektMas3.Models
{
    public abstract class Product
    {
        public int Waga { get; set; }
        public string Nazwa { get; set; }
    }
}
