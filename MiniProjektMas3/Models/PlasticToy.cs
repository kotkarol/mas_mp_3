﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MiniProjektMas3.Interfaces;

namespace MiniProjektMas3.Models
{
    public class PlasticToy : Plastic, IPlasticConvertible<PlasticMaterial>, IPlasticConvertible<PlasticCarPart>
    {

        public PlasticToy(int hardness, string color)
        {
            this.Hardness = hardness;
            this.Color = color;
        }

        public PlasticToy(int weight, int hardness, string color)
        {
            this.Weight = weight;
            this.Hardness = hardness;
            this.Color = color;
        }

        public PlasticMaterial ConvertToPlasticMaterial()
        {
            return new PlasticMaterial(this.Weight * 0.9, Hardness, Color);
        }


        public override int CalculateNeededAmountForUnit()
        {
            if (this.Hardness > 10)
            {
                return 5;
            }
            return 12;
        }

        PlasticMaterial[] IPlasticConvertible<PlasticMaterial>.Convert()
        {
            List<PlasticMaterial> pms = new List<PlasticMaterial>();
            PlasticMaterial pm = new PlasticMaterial(this.Hardness, this.Color);

            for (int i = 0; i < this.Weight / pm.CalculateNeededAmountForUnit(); i++)
            {
                pms.Add(new PlasticMaterial(pm.CalculateNeededAmountForUnit(), this.Hardness, this.Color));
            }

            return pms.ToArray();
        }

        PlasticCarPart[] IPlasticConvertible<PlasticCarPart>.Convert()
        {
            List<PlasticCarPart> pms = new List<PlasticCarPart>();
            PlasticCarPart pm = new PlasticCarPart(this.Hardness, this.Color);

            for (int i = 0; i < this.Weight / pm.CalculateNeededAmountForUnit(); i++)
            {
                pms.Add(new PlasticCarPart(pm.CalculateNeededAmountForUnit(), this.Hardness, this.Color));
            }

            return pms.ToArray();
        }
    }
}
