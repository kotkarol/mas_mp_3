﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MiniProjektMas3.Interfaces.IMaterial;
using MiniProjektMas3.Interfaces.IThing;

namespace MiniProjektMas3.Models
{
    public class GearBox : Steel, ICarPart, ITool
    {
        public override int CalculateNeededAmountForUnit()
        {
            return this.QualityLevel * 23;
        }
        public string CarPartName { get; set; }
        public string CarModel { get; set; }
        public string CarManufacturer { get; set; }
        public string Name { get; set; }
        public string Purpose { get; set; }
        public string ThingName { get; set; }
    }
}
