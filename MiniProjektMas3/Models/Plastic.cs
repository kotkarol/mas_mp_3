﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MiniProjektMas3.Interfaces;

namespace MiniProjektMas3.Models
{
    public abstract class Plastic : IPlastic
    {
        public abstract int CalculateNeededAmountForUnit();
        public int Hardness { get; set; }
        public int Weight { get; set; }
        public string Name { get; set; }
        public string Color { get; set; }
    }
}
