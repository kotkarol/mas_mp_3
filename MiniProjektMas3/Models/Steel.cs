﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MiniProjektMas3.Interfaces.IMaterial;

namespace MiniProjektMas3.Models
{
    public abstract class Steel : ISteel
    {
        public abstract int CalculateNeededAmountForUnit();
        public int QualityLevel { get; set; }
        public string Grade { get; set; }
        public int Weight { get; set; }
        public string Name { get; set; }
    }
}
