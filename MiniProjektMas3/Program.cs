﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Program.cs" company="">
//   
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MiniProjektMas3
{
    using System;

    using MiniProjektMas3.Interfaces;
    using MiniProjektMas3.Models;
    using MiniProjektMas3.Models.Dynamic;
    using MiniProjektMas3.Models.Material;
    using MiniProjektMas3.Wieleaspektowe;
    using MiniProjektMas3.Wielodziedziczenie;

    public class Program
    {
        static void Main(string[] args)
        {
            PlasticMaterial plasticMaterial = new PlasticMaterial(1000, 10, "red");
            SteelMaterial steelMaterial = new SteelMaterial()
            {
                Amount = 1000,
                Grade = "high",
                QualityLevel = 12
            };

            // Polimorfizm + dziedziczenie dynamiczne
            Plastic plasticCarPart = new PlasticCarPart(20, "black");
            Plastic plasticToy = new PlasticToy(20, "black");

            var plasticTool = new PlasticTool()
            {
                Color = "Black",
                Hardness = 10,
                Name = "Test",
                Weight = 10
            };

            // Wykorzystuje jego własciwoosci
            ITool overlappingTool = plasticTool;
            IPlastic overlappingPlastic = plasticTool;

            #region Overlapping
            try
            {
                ItSpeciallist speciallist = new ItSpeciallist("X")
                {
                    FavoritueSqlDialect = "PLSQL",
                    FavoritueIntelijExtension = "Any",
                    FavoritueFramework = "test"
                };
            }
            catch (Exception e)
            {
                Console.WriteLine("Pracownik nie ma przypsianych odpowednich rol");
            }
            ItSpeciallist itSpeciallist = new ItSpeciallist("Y");
            itSpeciallist.AddRole(Role.JavaProgrammer);
            itSpeciallist.AddRole(Role.JavascriptProgrammer);
            itSpeciallist.AddRole(Role.SqlProgrammer);

            itSpeciallist.FavoritueSqlDialect = "PLSQL";
            itSpeciallist.FavoritueIntelijExtension = "Any";
            itSpeciallist.FavoritueFramework = "test";
            #endregion

            #region Wielodziedziczenie
            OmnivorousAnimal omnivorousAnimal = new OmnivorousAnimal(PhysicalActivityLevel.High, 10, "Pig", 100, false);
            var consumption = omnivorousAnimal.GetAvarageDayConsumptionInKg();
            #endregion

            #region Dziedziczenie wieloaspektowe

            var srodekChemiczny = new CzyszczacySrodekChemiczny(
                "test",
                "superSklad",
                DateTime.Now,
                new OdczynZasadowy(1, "test", "test"),
                DateTime.Now,
                DateTime.Now);
            #endregion

            #region Dziedziczenie dynamiczne

            PlatformaSamochodowa platforma = new Ciezarowka(10.0 , 10.0 , 10.0 , 2);

            SamochodOsobowy samochodOsobowy = new SamochodOsobowy(platforma, 4);

            Dostawczak dostawczak = new Dostawczak(platforma, 100.0);

            #endregion

        }
    }
}
